/*
 * Spring 2019 - CSE 360 - Introduction to Software Engineering - Assignment II
 * This code is modified and enhanced as a part of Assignment 2 of CSE 360 -
 * Introduction to Software Engineering course at Arizona State University
 * offered by Dr. Debra Calliss. All the students in this class are subject to
 * ASU’s <a href="http://provost.asu.edu/academicintegrity">Academic Integrity
 * Policy</a> and should acquaint themselves with its content and requirements,
 * including a strict prohibition against plagiarism.
 * 
 * Student Name: Venkata Sai Sandeep Nadella 
 * Class ID: 502 
 * Assignment Number: 2
 * Description: This assignment is designed to help students get accustomed to
 * version control systems like git.
 */
package cse360assign2;

/**
 * This class implements Calculator functionality which can perform basic
 * calculations including addition, subtraction, multiplication,..
 * 
 * @author Debra Calliss
 * @author Sandeep Nadella
 */
public class Calculator {

	private int total;
	private String history;

	/**
	 * The Calculator constructor. The total and history are initialized to 0
	 * upon object creation.
	 * 
	 */
	public Calculator () {
		total = 0; // not needed - included for clarity
		history = "0";
	}

	/**
	 * Returns the total of all the operations
	 * 
	 * @return int The total value after the operations
	 */
	public int getTotal () {
		return total;
	}

	/**
	 * Performs add operation and updates the history
	 * 
	 * @param value The input value to be added to the total
	 */
	public void add (int value) {
		total += value;
		history += " + " + value; 
	}

	/**
	 * Performs subtraction operation and updates the history
	 * 
	 * @param value The input value to be subtracted from the total
	 */
	public void subtract (int value) {
		total -= value;
		history += " - " + value; 
	}

	/**
	 * Performs multiplication operation and updates the history
	 * 
	 * @param value The input value to be multiplied to the total
	 */
	public void multiply (int value) {
		total *= value;
		history += " * " + value; 
	}

	/**
	 * Performs division operation and updates the history
	 * 
	 * @param value The total value will be divided by the input value
	 */
	public void divide (int value) {
		if (value == 0) {
			total = 0;
		} else {
			total /= value;	
		}
		history += " / " + value;
	}

	/**
	 * Returns the complete history of all the operations
	 * 
	 * @return String
	 */
	public String getHistory () {
		return history;
	}
}